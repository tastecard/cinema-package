# Generic Cinema Package

## Set up

- Create a composer.json file in the root of your existing project, e.g. gourmet, with the following.

```
{
    "repositories": [
      {
        "url": "https://bitbucket.org/tastecard/cinema-package.git",
        "type": "vcs"
      }
    ],
    "require": {
      "dcg/cinema": "dev-master"
    },
    "autoload": {
        "psr-4": {
            "Dcg\\Cinema\\": "src/"
        }
    },
    "scripts": {
        "post-package-install": [
            "Dcg\\Cinema\\ConfigFileCreator::createConfigFile"
        ],
        "create-cinema-database-table" : [
            "Dcg\\Cinema\\Database::createCinemaCustomerTable"
        ]
    }
}
```

- After running composer install a config file should be created in the root of your existing project (config/cinema.php).
- Once you've populated the config file with the correct db details, run:
  composer create-cinema-database-table

## Get Cinema Token

```
    require '../vendor/autoload.php';
    $dcgCinemaUser = [
        'id' => $_SESSION['login_id'],
        'email' => $_SESSION['login_email'],
        'name' => $_SESSION['login_firstname'],
        'surname' => $_SESSION['login_surname'],
    ]; 
    $auth = new Cinema\AuthGateway($dcgCinemaUser);
    $token = $auth->authenticate();
```

- Once you have the token you can then redirect the user, e.g. 

```
    $redirector = new Cinema\Redirector($token, 'cinema_token_odeon');
    $redirector->redirectUser();
```