<?php

namespace Dcg\Cinema;
use \PDO;
use \PDOException;
use \Exception;

class DatabaseConnector {

    private static $dbh = null;

    /*
        Connect to platforms db - details defined in the cinema config file
    */
    public static function connect() 
    {   
        $configHelper = ConfigHelper::getInstance(true);

        if (is_null($configHelper)) {
            self::error('Problem getting config file.');
        }

        try {
            $dsn = 'mysql:host=' . $configHelper::getConfigValue('client_db_host', true) . ';dbname=' . $configHelper::getConfigValue('client_db_name', true);
            $user = $configHelper::getConfigValue('client_db_username', true);
            $password = $configHelper::getConfigValue('client_db_password', true);

            self::$dbh = new PDO($dsn, $user, $password);
        } catch (PDOException $e) {
            self::error('Problem connecting to Database. ' . $e->getMessage());
        }
    }


    /**
     *  Return the database instance
     * @return self
     */
    public static function getDbInstance()
    {
        if (is_null(self::$dbh)) {
            self::connect();
        }
        return self::$dbh;
    }

    /**
     *  Handle error, write to log and throw exception
     * @param string $message
     */
    public static function error($message)
    {
        error_log($message);
        throw new Exception($message);
    }

}