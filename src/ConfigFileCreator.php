<?php

namespace Dcg\Cinema;

class ConfigFileCreator {
    
    // file paths relative to the root project
    
    /**
     * @var string
     */
    private static $configFilePath = "config/cinema.php";
    
    /**
     * @var string
     */
    private static $exampleConfigFilePath = "vendor/dcg/cinema/config/cinema.php";

    /**
     *  Copy package's config file to project
     */
    public static function createConfigFile () 
    {
        $configFile = self::$configFilePath;
        if (!file_exists($configFile)) {
            copy(self::$exampleConfigFilePath, $configFile);
        }
    }

}