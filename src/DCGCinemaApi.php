<?php

namespace Dcg\Cinema;
use \Exception;

class DCGCinemaApi {
    
    /**
     * @var boolean
     */
    private $test = true;

    /**
     * @var string
     */
    private $version = '';

    /**
     * @var string
     */
    private $production_url = '';

    /**
     * @var string
     */
    private $staging_url = '';
    
    /**
     * @var string
     */
    private $client_token;

    /**
     * @var string
     */
    private $user_token;

    /**
     * @var array
     */
    private $config = [];
    
    public function __construct()
    {
        $this->setApiSettings();
    }

    /**
     *  Set api properties based on config
     */
    private function setApiSettings()
    {
        $configHelper = ConfigHelper::getInstance();

        if (is_null($configHelper) || !sizeof($configHelper::getConfigValues())) {
            throw new Exception("Problem loading config");
        }

        $this->client_token = $configHelper::getConfigValue('client_token', true);
        $this->production_url = $configHelper::getConfigValue('production_api_url', true);
        $this->staging_url = $configHelper::getConfigValue('staging_api_url');
        $this->version = $configHelper::getConfigValue('version');
        $this->test = $configHelper::getConfigValue('is_test', true);
    }
    
    /**
     *  Get Base url
     * @return string
     */
    private function getBaseUrl()
    {
        return $this->test ? $this->staging_url : $this->production_url;
    }
    
    /**
     *  Build ful url
     * @return string
     */
    private function buildUrl($endpoint)
    {
        return $this->getBaseUrl() . '/' . $this->version . '/' . trim($endpoint, '/');
    }
    
    /**
     *  Headers for API request
     * @param array $headers
     * @param string $method
     * @return array
     */
    private function buildHeaders($headers, $method = 'GET')
    {
        $base_headers = [
            'Client-Token: ' . $this->client_token,
        ];
        
        if ($method != 'GET') {
            $base_headers[] = 'Content-Type: application/json';
        }
        
        if ($this->user_token) {
            $base_headers['User-Token'] = $this->user_token;
        }
        
        return array_merge($base_headers, $headers);
    }
    
    /**
     *  Make POST request
     * @param string $endpoint
     * @param array|null $data
     * @param array $headers
     * @return array
     */
    public function post($endpoint, $data = null, $headers = [])
    {
        return $this->call('POST', $endpoint, $data, $headers);
    }
    
    /**
     *  Make GET request
     * @param string $endpoint
     * @param array|null $data
     * @param array $headers
     * @return array
     */
    public function get($endpoint, $data = null, $headers = [])
    {
        return $this->call('GET', $endpoint, $data, $headers);
    }
    
    /**
     *  Actual API request
     * @param string $method
     * @param string $endpoint
     * @param array|null $data
     * @param array $headers
     * @return array
     */
    public function call($method, $endpoint, $data = null, $headers = [])
    {
        $method = strtoupper($method);
        
        $url = $this->buildUrl($endpoint);
        
        $headers = $this->buildHeaders($headers, $method);
        
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        if ($data) {
            if ($method == 'GET') {
                $url .= '?' . http_build_query($data);
                
            } else {
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                
            }
        }
        
        curl_setopt($ch, CURLOPT_URL, $url);
        if ($this->test) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $response_raw = curl_exec($ch);
        $info = curl_getinfo($ch);
        
        $status_code = $info['http_code'];
        
        $response = json_decode($response_raw, true);
        
        return [
            'successful' => $status_code >= 200 && $status_code <= 299,
            'status_code' => $status_code,
            'content' => $response,
            'response_raw' => $response_raw
        ];
    }
}
