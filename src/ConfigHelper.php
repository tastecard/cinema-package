<?php

namespace Dcg\Cinema;
use \Exception;

class ConfigHelper {
    
    /**
     * @var array
     */
    private static $configFilePaths = [

        'relative_to_root' => [
            'config_file_path' => 'config/cinema.php',
            'example_file_path' => 'vendor/dcg/cinema/config/cinema.php'
        ],

        'relative_to_package' => [
            'config_file_path' => '../../../../config/cinema.php',
            'example_file_path' => '../config/cinema.php'
        ]

    ];

    /**
     * @var boolean
     */
    private static $useRootComposerRelPath = false;

    /**
     * @var array
     */
    protected static $configValues = [];

    /**
     * @var self
     */
    protected static $instance = null;

    /**
     * singleton: return self
     *
     * @param boolean $useRootComposerRelPath
     * @return self
     */
    public static function getInstance ($useRootComposerRelPath = false) 
    {
        if (is_null(self::$instance)) {
            self::setInstance();
        }
        self::$useRootComposerRelPath = $useRootComposerRelPath;
        self::init();

        return self::$instance;
    }

    /**
     *  Set singleton instance
     */
    private static function setInstance () 
    {
        self::$instance = new self();
    }

    /**
     *  Get values from config file
     */
    private static function init ()
    {
        self::configFileToArray();
    }

    /**
     *  Get values from config file
     */
    public static function configFileToArray () 
    {  
        $filePath = self::$useRootComposerRelPath ? self::$configFilePaths['relative_to_root']['config_file_path']  : self::$configFilePaths['relative_to_package']['config_file_path'];

        if (!file_exists($filePath)) {
            // set as original base config
            $filePath = self::$useRootComposerRelPath ? self::$configFilePaths['relative_to_root']['example_file_path']  : self::$configFilePaths['relative_to_package']['example_file_path'];
        }

        if (file_exists($filePath)) {
            self::$configValues = require $filePath;
        } else {
            error_log('Problem reading config file');
        }
    }

    /**
     *  Gets the values that were in the config
     * @return array
     */
    public static function getConfigValues ()
    {
        return self::$configValues;
    }

    /**
     *  Gets specific key fom config
     * @return string
     */
    public static function getConfigValue ($key, $imperative = false) 
    {
        if (!isset(self::$configValues[$key]) || (empty(self::$configValues[$key]) && !(self::$configValues[$key] === false))) {
            if ($imperative) {
                throw new Exception($key. " is not set in the config file.");
            }
            return null;
        }
        return self::$configValues[$key];
    }

}