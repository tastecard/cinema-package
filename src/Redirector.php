<?php

namespace Dcg\Cinema;
use \Exception;

class Redirector {
    
    /**
     * @var string
     */
    protected $userToken;

    /**
     * @var string
     */
    protected $chain;

    /**
     * @var ConfigHelper
     */
    protected static $configHelper;

    /**
     * Create new Redirector instance
     * @param string $userToken
     * @param string $chain
     */
    public function __construct($userToken, $chain)
    {
        $this->userToken = $userToken;
        $this->chain = $chain;
        self::$configHelper = ConfigHelper::getInstance();

        if (is_null(self::$configHelper)) {
            throw new Exception("Problem loading config");
        }
    }

    /**
     *  Redirect user to cinema site
     * @param array $parameters
     */
    public function redirectUser(array $parameters = array())
    {
        $url = $this->getBaseUrl() . '/'. $this->getChainToken() .'/' . $this->userToken;

        if ($parameters) {
            $parameterString = array_map(function($key, $value) {
                if (is_bool($value)) {
                    $value = (int) $value;
                }
                return "$key=" . urlencode($value);
            }, array_keys($parameters), array_values($parameters));

            $url .= '?' . implode('&', $parameterString);
        }

        header("Location:" . $url);
        die();
    }   

    /**
     *  Get correct url from config based on is_test config value
     * @return string
     */
    public function getBaseUrl()
    {
        $configKey = self::$configHelper->getConfigValue('is_test') ? 'staging_site_url' : 'production_site_url';
        return self::$configHelper->getConfigValue($configKey, true);
    }

    /**
     *  Get token from config
     * @return string
     */
    public function getChainToken()
    {
        return self::$configHelper->getConfigValue($this->chain, true);
    }

}