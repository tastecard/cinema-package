<?php

namespace Dcg\Cinema;
use \Exception;

class AuthGateway {
    
    /**
     * @var DCGCinemaApi
     */
    private $cinemaApi = null;

    /**
     * @var string
     */
    protected $authEndPoint = 'users/';

     /**
     * @var array
     */
    protected $clientUser = [];

    /**
     * Create a new AuthGateway instance.
     *
     * @param array
     * @return void
     */
    public function __construct($clientUser)
    {
        $this->cinemaApi = new DCGCinemaApi();
        $this->clientUser = $clientUser;
    }

    /**
     *  Authenticate user and return a token for cinema platform
     * @return null|string
     */
    public function authenticate()
    {
        $token = null;
        $dcgCinemaUser = $this->checkIfUserHasCinemaAccount($this->clientUser['id']);
        
        if (!$dcgCinemaUser) {
            // dont have the user id - try and create the user
            $createUserResponse = $this->createUserInCinemaApi();

            if (!is_null($createUserResponse)) {
            
                $dcgCinemaUser = [
                    'customer_account_id' => $this->clientUser['id'],
                    'dcg_cinema_user_id' => $createUserResponse['id'],
                    'dcg_cinema_email' => $createUserResponse['email'],
                    'dcg_cinema_firstname' => $createUserResponse['firstname'],
                    'dcg_cinema_surname' => $createUserResponse['surname'],
                ];
                
                // Save these user details
                Database::insertCinemaUser($dcgCinemaUser);
                
            } else {
                // Should never hit this error
                throw new Exception('DCG Cinema API Error: Empty user.');
            }

        }
        // If we have a user, create a user token
        if (isset($dcgCinemaUser['dcg_cinema_user_id']) && $dcgCinemaUser['dcg_cinema_user_id']) {
            $response = $this->cinemaApi->post($this->authEndPoint ."{$dcgCinemaUser['dcg_cinema_user_id']}/tokens");
            
            if ($response['successful'] && $response['content']['data']['token']) {
                $token = $response['content']['data']['token'];
            }
            
        }
        return $token;
    }

    /**
     *  Checks if user already has an account on the cinema platform
     * @return null|array
     */
    public function checkIfUserHasCinemaAccount($clientUserId, $params = null)
    {
        $dcgCinemaUser = null;
        if ($dcgCinemaUser = Database::getCinemaUser($clientUserId)) {
            // We have the user id - Does the user exist?
            $response = $this->cinemaApi->get($this->authEndPoint . $dcgCinemaUser['dcg_cinema_user_id']);
            
            if (! $response['successful']) {
                // Can't find user on DCG Cinema platform
                $dcgCinemaUser = null;
            }
        }
        return $dcgCinemaUser;
    }

    /**
     *  Create account for this user on the cinema platform
     * @return null|array
     */
    public function createUserInCinemaApi()
    {
        $dcgCinemaUser = null;
        $response = $this->cinemaApi->post($this->authEndPoint, [
            'email' => $this->clientUser['email'],
            'firstname' => $this->clientUser['name'] ?: '--',
            'surname' => $this->clientUser['surname'],
        ]);

        if ($response['successful']) {
            // User created
            $dcgCinemaUser = $response['content']['data'];
        } elseif ($response['status_code'] == 409) {
            // User already exists
            // Search for user here
            $response = $this->cinemaApi->get($this->authEndPoint, ['email' => $this->clientUser['email']]);
            
            if ($response['successful']) {
                // We have found the user
                $dcgCinemaUser = $response['content']['data'][0];
            } else {
                // Couldn't find the user by email
                // In theory, this should never happen in the case of a 409
                throw new Exception('DCG Cinema API Error: User already exists.');
            }
        } else {
            // Error
            throw new Exception('DCG Cinema API Error: Unable to create user.');
        }
        return $dcgCinemaUser;
    }

}