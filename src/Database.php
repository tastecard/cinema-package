<?php

namespace Dcg\Cinema;
use \PDO;
use \Exception;

class Database {
    
    /**
     * @var string
     */
    private static $cinemaCustomerTableName = "customer_accounts_dcg_cinema_accounts";

    /**
     * create table (if it doesnt exist) to hold cinema details - including cinema user id
     */
    public static function createCinemaCustomerTable()
    {
        $dbInstance = DatabaseConnector::getDbInstance();

        if (is_null($dbInstance)) {
            error_log('Problem connecting to Database.');
            throw new Exception("Problem connecting to Database.");
        }

        $stmt = $dbInstance->prepare("CREATE TABLE IF NOT EXISTS `".self::$cinemaCustomerTableName."` (
            `customer_account_id` INT(11) NOT NULL,
            `dcg_cinema_user_id` CHAR(36) NULL DEFAULT NULL,
            `dcg_cinema_email` VARCHAR(255) NULL DEFAULT NULL,
            `dcg_cinema_firstname` VARCHAR(255) NULL DEFAULT NULL,
            `dcg_cinema_surname` VARCHAR(255) NULL DEFAULT NULL,
            `created_timestamp` DATETIME NULL DEFAULT NULL,
            `updated_timestamp` DATETIME NULL DEFAULT NULL,
            PRIMARY KEY (`customer_account_id`),
            INDEX `dcg_cinema_user_id` (`dcg_cinema_user_id`),
            INDEX `dcg_cinema_email` (`dcg_cinema_email`)
        )");
        
        $stmt->execute();
    }

    /**
     * Get the DCG details of a user
     * @param  integer    $customer_account_id
     * @return null|array
     */
    public static function getCinemaUser($customerAccountId)
    {
        $dbInstance = DatabaseConnector::getDbInstance();

        $stmt = $dbInstance->prepare("SELECT * FROM `".self::$cinemaCustomerTableName."` WHERE `customer_account_id` = ?");
        $stmt->execute([$customerAccountId]);
        
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    
    /**
     * Save the DCG Cinema details of a user
     * @param  array   $user he user details
     * @return boolean
     */
    public static function insertCinemaUser($user)
    {   
        $dbInstance = DatabaseConnector::getDbInstance();

        $stmt = $dbInstance->prepare("REPLACE INTO `".self::$cinemaCustomerTableName."` (
            `customer_account_id`,
            `dcg_cinema_user_id`,
            `dcg_cinema_email`,
            `dcg_cinema_firstname`,
            `dcg_cinema_surname`,
            `created_timestamp`,
            `updated_timestamp`
        ) VALUES (
            :customer_account_id,
            :dcg_cinema_user_id,
            :dcg_cinema_email,
            :dcg_cinema_firstname,
            :dcg_cinema_surname,
            NOW(),
            NOW()
        )");
        
        return $stmt->execute([
            'customer_account_id' => $user['customer_account_id'],
            'dcg_cinema_user_id' => $user['dcg_cinema_user_id'],
            'dcg_cinema_email' => $user['dcg_cinema_email'],
            'dcg_cinema_firstname' => $user['dcg_cinema_firstname'],
            'dcg_cinema_surname' => $user['dcg_cinema_surname'],
        ]);
    }

}