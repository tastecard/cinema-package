<?php

return [

	'client_token' => '',

	'cinema_token_odeon' => '',

	'cinema_token_vue' => '',

	'cinema_token_cineworld' => '',

	'cinema_token_showcase' => '',

	'is_test' => true,

	'staging_api_url' => '',

	'production_api_url' => '',

	'version' => 'v1',

	'staging_site_url' => '',

	'production_site_url' => '',

	/*

		Database

	*/

	'client_db_host' => '',

	'client_db_username' => '',

	'client_db_password' => '',

	'client_db_name' => ''
	
];